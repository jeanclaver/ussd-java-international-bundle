/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundles;


import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import ucipclient.UCIPClientEngine;
import ucipclient.UCIPGetAccountDetailsResponse;
import ucipclient.UCIPUpdateBalanceAndDateResponse;
import ucipclient.UCIPUpdateOfferResponse;
import mtn.gn.com.data.Bundle;
import mtn.gn.com.data.DataDA;


/**
 *
 * @author Administrateur
 */
public class ProvisioningEngine {
    
    private static int[] supportedServiceClasses = {1, 2, 3, 4, 6, 15, 20, 28, 30, 31, 19, 102, 66, 40};
    
     
    ResponseEnum execute(String msisdn, String transactionID, Bundle bundle, String intBundle){
        
        ResponseEnum respEnum  = ResponseEnum.SUCCESS;      
        MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionID+"|BundleName:"+bundle.getBundleName()+"| Fetching Service Class ");
        UCIPClientEngine ucipEngine = new UCIPClientEngine();
        UCIPGetAccountDetailsResponse getAccountResponse = ucipEngine.GetAccountDetails(msisdn, transactionID);
        if(getAccountResponse != null){
            int sc = getAccountResponse.getServiceClass();
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionID+"|BundleName:"+bundle.getBundleName()+"|Service Class="+sc);
        
            if(this.IsServiceClassAllowed(sc)){
                double balance = getAccountResponse.getBalance();                
                int price = bundle.getPrice();                
                if(price > balance){
                  respEnum = ResponseEnum.BALANCEINSUFUSANT;
                }else{
                                 
                    UCIPUpdateBalanceAndDateResponse deductMAResponse = ucipEngine.UpdateMainAccountBalanceAndDate(msisdn, transactionID, -price,true, new Date(), intBundle);                    
                    int x = deductMAResponse.getResponseCode();                    
                    if((x == 0) && (!deductMAResponse.isError())){
                        java.util.Calendar cal =java.util.Calendar.getInstance();        
                        cal.add(java.util.Calendar.DATE,bundle.getValidity()); // this will add validity days        
                        Date expiry =  cal.getTime();
                        UCIPUpdateBalanceAndDateResponse refillDAResponse = ucipEngine.UpdateDedicatedAccountBalanceAndDate(msisdn, transactionID, bundle.getDa(), bundle.getRefill(),true, expiry, intBundle);        
                        int y = refillDAResponse.getResponseCode();        
                        if((y == 0) && (!refillDAResponse.isError())){
                            respEnum  = ResponseEnum.SUCCESS;
                            DataDA dataDa = new DataDA();
                            int bonus = 0;
                            int msisdnInt = Integer.parseInt(msisdn);
                            //msisdn,String bundleName, String bundleType,  int price,int refill, int validity, int da, String transaction_id, String response
                            dataDa.insertTransaction(msisdnInt, bundle.getBundleName(), bundle.getBundleType(),  bundle.getPrice(), bundle.getRefill(), bundle.getValidity(), bundle.getDa(), transactionID, respEnum.toString(), bonus);
                        }else {            
                            respEnum  = ResponseEnum.ERROR;                                
                        }
                    }else{
                        respEnum  = ResponseEnum.ERROR;                      
                    }                                    
                }
            
            }else {
                MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionID+"|"+bundle.getBundleName()+"| Not allowed");
                
                respEnum  = ResponseEnum.NOT_ALLOWED;
            }
        }else {
            MLogger.Log(this, LogLevel.ALL, "Subscriber:"+msisdn+"|Session:"+transactionID+"|"+bundle.getBundleName()+"|Failed to get account details");
                 
            respEnum  = ResponseEnum.ERROR; 
        }       
        
        return respEnum;    
    }
    
    private boolean IsServiceClassAllowed(int sc){
        for(int n : supportedServiceClasses){
            if(n==sc)
                return true;
        }
        
        return false;
    }
     
}
