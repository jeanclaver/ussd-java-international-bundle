/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundles;
import java.io.Serializable;
import mtngc.ussd.*;
/**
 *
 * @author EKhosa
 */
public class BundleSession  {
    private String ussdSessionId;
    private RequestEnum selection;
    private int step;
    private String req;

    /**
     * @return the ussdSessionId
     */
    public String getUssdSessionId() {
        return ussdSessionId;
    }

    /**
     * @param the ussdSessionId to set
     */
    public void setUssdSessionId(String ussdSessionId) {
        this.ussdSessionId = ussdSessionId;
    }

    /**
     * @return the selection
     */
    public RequestEnum getSelection() {
        return selection;
    }

    /**
     * @param the selection to set
     */
    public void setSelection(RequestEnum selection) {
        this.selection = selection;
    }

    /**
     * @return the step
     */
    public int getStep() {
        return step;
    }

    /**
     * @param  the step to set
     */
    public void setStep(int step) {
        this.step = step;
    }

    /**
     * @return the req
     */
    public String getReq() {
        return req;
    }

    /**
     * @param req the req to set
     */
    public void setReq(String req) {
        this.req = req;
    }
       
}
