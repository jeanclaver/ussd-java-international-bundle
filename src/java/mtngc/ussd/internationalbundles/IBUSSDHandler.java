/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundles;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse ;
//import javax.servlet.http.HttpSession ;
import javax.servlet.ServletContext ;
import java.io.*;
import java.util.*;
import mtn.gn.com.data.Bundle;
import mtn.gn.com.data.DataDA;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;

import org.apache.commons.lang.StringUtils;
/**
 *
 * @author ekhosa
 */
public class IBUSSDHandler  {
    
    static String ContextKeyName;
    static ArrayList <Integer> supportedMSISDNs;
    
    static
    {  
        ContextKeyName = "IntBundle";
        String filePath = "C:\\Logs\\"+ContextKeyName+"_v1";
        try{
        File file = new File(filePath);
        if (!file.exists()) {
            if (file.mkdir()) {
                System.out.println("Directory is created!");
            } else {
                System.out.println("Failed to create directory!");
            }
        }
        }catch(Exception e){
            e.printStackTrace();
        }
        MLogger.setHomeDirectory(filePath);
    }
    
    public void handleRequest(HttpServletRequest request, HttpServletResponse response){
        TraceParametersAndHeaders(request);
            
        try {
            String msisdnStr = request.getParameter("MSISDN");
            //msisdnStr = "224660134178";
            int msisdn = 0;
            try{msisdnStr = msisdnStr.substring(3);}catch(Exception n){};
            try{msisdn = Integer.parseInt(msisdnStr);}catch(Exception n){};
            
            if(IsMSISDNAllowed(msisdn)){
                processRequest(msisdnStr,request, response);
            }else {
                response.setContentType("UTF-8");
                response.setHeader("Freeflow", "FB");
                response.setHeader("cpRefId", "emk2545");
                PrintWriter out = response.getWriter();
                out.append("Le service n'est pas disponible");           
                out.close();            
            }
            
        } catch (Exception e) {
            MLogger.Log(this, e);
        }    
    }
    
    public void processRequest(String msisdn,HttpServletRequest request, HttpServletResponse response){
        String FreeFlow = "FC";
        try {
            
            String ussdSessionid = request.getParameter("SESSIONID");            
            String input = request.getParameter("INPUT");

            ServletContext context = request.getServletContext();

                StringBuilder sb = new StringBuilder();
             
                MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing request.");
                            
                String contextKey = GetContextKey(ussdSessionid);
                Object obj = context.getAttribute(contextKey);
                if(obj == null){
                    if(input != null){
                        String str = displayMainMenu(context, ussdSessionid, msisdn);
                        sb.append(str );
                    }else
                        sb.append("Application is running");
                }else {
                    
                    BundleSession bundleSession = (BundleSession) obj;
                    int step = bundleSession.getStep();
                switch (step) {
                    case 1:
                        {
                            // if previous screen was the main menu
                            String str = processMainMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str);
                            break;
                        }
                    case 2:
                        {
                            // if previous screen was the data bundle menu (bundle name) menu
                            String str = processConfirmMenuInput(input,  msisdn,  ussdSessionid, bundleSession, context);
                            sb.append(str);
                            break;
                        }
                    default:
                        {
                            // if previous step is not supported
                            String str = displayMainMenu(context, ussdSessionid, msisdn);
                            sb.append(str);
                            break;
                        }
                }
                }
                
            //}
            response.setContentType("UTF-8");
            response.setHeader("Freeflow", FreeFlow);
            response.setHeader("cpRefId", "emk2545");
            //response.setContentType(type);
            PrintWriter out = response.getWriter();
//            out.append("Your string goes here\n");
//            out.append("Your string goes here");
            
            out.append(sb.toString());
            out.close();
        } catch (Exception e) {
            MLogger.Log(this, e);
        }
    }
    protected String displayMainMenu(ServletContext context, String ussdSessionid, String msisdn){
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"|Displaying Package list");
        BundleSession bundleSession = new BundleSession();
        String contextKey = GetContextKey(ussdSessionid);
        Object obj = context.getAttribute(contextKey);
        
        if(obj != null){
            context.removeAttribute(contextKey);
        }
        bundleSession.setUssdSessionId(ussdSessionid);
        
        bundleSession.setStep(1);

        context.setAttribute(contextKey, bundleSession);
        MainMenu menu = new MainMenu();
        String str = menu.getString();
        
        return str;
    }
    
    public String processMainMenuInput(String input, String msisdn, String ussdSessionid,BundleSession bundleSession,ServletContext context){
        String msg = null;
        
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim())) ){
                      
            input =  input.trim();
            //int packageTypeIndex = Integer.parseInt(input);
            String  req = null;
            
            if(input.equals("1")){
                req = "M";
                //
            }else if(input.equals("2")){
                req = "D";
            }
            ConfirmMenu confirmMenu = new ConfirmMenu(req);            
            msg = confirmMenu.getString();
            
            bundleSession.setStep(2);
            bundleSession.setReq(req);
            String contextKey = GetContextKey(ussdSessionid);
            context.setAttribute(contextKey, bundleSession);                                  
        }
        else{
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }    
        return msg;
    }
        
    public String processConfirmMenuInput(String input, String msisdn, String ussdSessionid, BundleSession bundleSession,ServletContext context){
        String msg = null;
        
        MLogger.Log(this, LogLevel.DEBUG, "Processing confirmation input "+input);
        if((input != null) && ( !input.trim().equals("")) && (input.trim().length() == 1) && (StringUtils.isNumeric(input.trim())) ){
            input =  input.trim();
            
            String  req = bundleSession.getReq();
            MLogger.Log(this, LogLevel.DEBUG, "RequestEnum "+req.toString());
            DataDA dataDa = new DataDA();
            List<Bundle> list = dataDa.getBundles(req);
            Bundle selectionBundle = null;
            int i=1;
            for(Bundle bundle : list){
               if(input.equals(i+"")){
                   selectionBundle = bundle;
               }
                i++;
            }
            if(selectionBundle != null){
                ProvisioningEngine provengin = new ProvisioningEngine();
                ResponseEnum respEnum = provengin.execute(msisdn, ussdSessionid, selectionBundle, ContextKeyName);
                if(respEnum == ResponseEnum.SUCCESS){
                   msg = "Felicitation! Vous avez active avec succes le forfait "+selectionBundle.getBundleName()+" valable "+selectionBundle.getValidity()+" jours.\n";
                }else if(respEnum == ResponseEnum.NOT_ALLOWED){
                   msg = "Cher client, vous n etes pas autorise a utiliser ce service. Pour plus d informations, appelez le 111. Internet : jusqu a 100% bonus offert. Tapez *100*3#.\n";
                }else if(respEnum == ResponseEnum.BALANCEINSUFUSANT){//
                   msg = "Cher client, vous n avez pas assez de credit pour souscrire au forfait "+selectionBundle.getBundleName()+". Veuillez recharger votre compte et reessayez.\n";
                }else{
                    msg = "Il y'a erreur.\n";
                }
            }else{
                msg = displayMainMenu(context, ussdSessionid, msisdn);
            }
               
            String contextKey = GetContextKey(ussdSessionid);
            context.removeAttribute(contextKey);
                                  
        }else if(input.equals("0")){
            //naughty user, he entered wrong information
            msg = displayMainMenu(context, ussdSessionid, msisdn);
        }else{
             msg = displayMainMenu(context, ussdSessionid, msisdn);
        }    
        MLogger.Log(this, LogLevel.DEBUG, msg);
        return msg;
    }
    
    private void processShortCut(String msisdn, String ussdSessionid,RequestEnum requestEnum, ServletContext context){
        BundleSession bundleSession = new BundleSession();
        MLogger.Log(this, LogLevel.DEBUG, "Subscriber:"+msisdn+"|Session "+ussdSessionid+"| Processing Shortcut request|RequestEnum = "+requestEnum.name());
        bundleSession.setUssdSessionId(ussdSessionid);
        bundleSession.setStep(2);
        bundleSession.setSelection(requestEnum);
        String contextKey = GetContextKey(ussdSessionid);
        context.setAttribute(contextKey, bundleSession);
        
    }
    
    protected void TraceParametersAndHeaders(HttpServletRequest request){
        
        Enumeration<String> headerEnum = request.getHeaderNames();
        MLogger.Log(this, LogLevel.DEBUG, "Tracing HTTP Headers and Parameters ");
        
        while(headerEnum.hasMoreElements()) {
            String headerName = headerEnum.nextElement();
            String headerValue = request.getHeader(headerName);
            //System.out.println("Header:- "+headerName+": "+headerValue);
            MLogger.Log(this, LogLevel.DEBUG, "Header:- "+headerName+": "+headerValue);
        } 
        
        Map<String, String[]> map = request.getParameterMap();
        //Reading the Map
        //Works for GET && POST Method
        for(String paramName:map.keySet()) {
            String[] paramValues = map.get(paramName);

            //Get Values of Param Name
            for(String valueOfParam:paramValues) {
                //Output the Values
                //System.out.println("Value of Param with Name "+paramName+": "+valueOfParam);
                //ystem.out.println("Parameter:- "+paramName+": "+valueOfParam);
                MLogger.Log(this, LogLevel.DEBUG, "Parameter:- "+paramName+": "+valueOfParam);
            }
        }           
        
    }
    
    protected String GetContextKey(String ussdSessionid){
        if (ContextKeyName == null)
            throw new RuntimeException("ContextKeyName not set");
        return ContextKeyName+"-"+ussdSessionid;
    } 
    
    protected boolean IsMSISDNAllowed(int msisdn){
        if((supportedMSISDNs == null) || (supportedMSISDNs.size() == 0))
            return true;
        if(msisdn == 0)
            return true;
        for(int n : supportedMSISDNs){
            if(n==msisdn)
                return true;
        }
        
        return false;
    }
}
