/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundles;
import java.util.ArrayList;
import mtn.gn.com.data.Bundle;
import mtn.gn.com.data.DataDA;

import java.util.Enumeration;
import java.util.List;
import mtngc.bundles.core.UserOption;

/**
 *
 * @author EKhosa
 */
public class ConfirmMenu {
    private String req;

    public ConfirmMenu(String req){
        this.req=req;
    }
    
    

    public String getString(){
        StringBuilder sb = new StringBuilder(); 
        
        DataDA dataDa = new DataDA();
        sb.append("Confirmez vous l achat des forfaits suivants:\n");
        List<Bundle> list = dataDa.getBundles(req); 
        int i=1;
        for(Bundle bundle : list){
            
            sb.append(i+". "+bundle.getBundleName()+" "+bundle.getDuration()+" a "+bundle.getPrice()+"F ("+bundle.getValidity()+"jrs)\n");
            i++;
        }
        sb.append("# Retour\n");
        sb.append("Repondez");
         
        String str = sb.toString();
                
        return str;
    }

   
    
    

    
}
