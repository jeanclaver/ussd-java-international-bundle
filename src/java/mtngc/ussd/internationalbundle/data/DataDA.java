/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundle.data;
import dbaccess.dbsrver.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import mtnz.util.logging.LogLevel;
import mtnz.util.logging.MLogger;
import java.util.*;
/**
 *
 * @author EKhosa
 */
public class DataDA extends AbstractDBSrverClient {
   
    
    public int insertTransaction (int msisdn, String transacation_id, String bundleType, String bundleName, int price,int refill, int validity, int da,String response) {
                
        int resp = 0;
        Connection conn = null;
        PreparedStatement pstmt =  null;
        
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder();
            
            sb.append("INSERT INTO  ");
            sb.append("INTERNATIONALBUNDLE_TRANSACT ( ");
            sb.append("msisdn,bundleName,bundleType,price,refill,validity,da,transaction_id, response)");
            sb.append("VALUES (   ?,     ?,      ?,     ?,    ?,    ?,   ?,    ?,  ?) ");
                                   
            String sqlStr = sb.toString();
            
            pstmt = conn.prepareStatement(sqlStr);
            pstmt.setInt(1, msisdn);
            pstmt.setString(2, bundleName);
            pstmt.setString(3, bundleType);
            pstmt.setInt(4, price);
            pstmt.setInt (5, refill);
            pstmt.setInt (6, validity); 
            pstmt.setInt (7, da);
            pstmt.setString (8, transacation_id);
            pstmt.setString(9, response);
            
            // execute insert SQL stetement
            pstmt .executeUpdate();
           

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{pstmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return resp;
    }  
    
    public List<Bundle> getBundles(String bundle_type) {
        List<Bundle> list = new ArrayList<Bundle>();
        
        
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try{
            conn = getConnection();
            StringBuilder sb = new StringBuilder(); 
//            select * from INTERNATIONALBUNDLE_BUNDLE
//            where BUNDLE_TYPE = 'D'
                     
            sb.append("select * from INTERNATIONALBUNDLE_BUNDLE "); 
            sb.append("where BUNDLE_TYPE ='"+bundle_type+"'");
            
            String sqlStr = sb.toString();
            stmt = conn.createStatement();
            rs = stmt.executeQuery(sqlStr);

            while( rs.next() )
            {
                Bundle dc = new Bundle();
                //dc.setPackageType(packageType);
                String bundleName = rs.getString("BUNDLE_NAME");
                dc.setBundleName(bundleName);
                String bundleType = rs.getString("BUNDLE_TYPE");
                dc.setBundleType(bundleType);
                int price = rs.getInt("PRICE");
                dc.setPrice(price);
                int refill = rs.getInt("REFILL");
                dc.setPrice(refill);  
                int validity = rs.getInt("VALIDITY");
                dc.setValidity(validity);
                int da = rs.getInt("DA");
                dc.setValidity(da);  
              
                list.add(dc);
            }

        }catch(Exception e){
            MLogger.Log(this, e);
        }finally{
            try{rs.close();}catch(Exception e){}
            try{stmt.close();}catch(Exception e){}
            try{conn.close();}catch(Exception e){}
            
        }
        return list;
    }
    
}
