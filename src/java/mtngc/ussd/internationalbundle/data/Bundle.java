/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mtngc.ussd.internationalbundle.data;

import java.io.Serializable;

/**
 *
 * @author EKhosa
 */
public class Bundle implements Serializable {
    
    private String bundleName;
    private String bundleType;
    private int price;
    private int refill;
    private int validity;
    private int da;

    /**
     * @return the bundleName
     */
    public String getBundleName() {
        return bundleName;
    }

    /**
     * @param bundleName the bundleName to set
     */
    public void setBundleName(String bundleName) {
        this.bundleName = bundleName;
    }

    /**
     * @return the bundleType
     */
    public String getBundleType() {
        return bundleType;
    }

    /**
     * @param bundleType the bundleType to set
     */
    public void setBundleType(String bundleType) {
        this.bundleType = bundleType;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    /**
     * @return the refill
     */
    public int getRefill() {
        return refill;
    }

    /**
     * @param refill the refill to set
     */
    public void setRefill(int refill) {
        this.refill = refill;
    }

    /**
     * @return the validity
     */
    public int getValidity() {
        return validity;
    }

    /**
     * @param validity the validity to set
     */
    public void setValidity(int validity) {
        this.validity = validity;
    }

    /**
     * @return the da
     */
    public int getDa() {
        return da;
    }

    /**
     * @param da the da to set
     */
    public void setDa(int da) {
        this.da = da;
    }
    
   
}
